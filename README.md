Testing changes in a merge. Could you spot where `changes from a merge`
was introduced?

Find it by:

    git diff 3d21b51e4b287a81379748595acd585ddcff9a74..ac12d1985ff8fd4adf634f7ac4ef2ba37ba4bdcb

But you can't find it by `git log`:

    git log -p 3d21b51e4b287a81379748595acd585ddcff9a74..ac12d1985ff8fd4adf634f7ac4ef2ba37ba4bdcb

Conclusion: Never try to make changes in a merge?
